package com.it.tbh.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author TBH
 * @date 2025-02-06 17:21:59
 * @desc
 */
@Data
@NoArgsConstructor
public class Policy implements Serializable {

    @JsonProperty("Statement")
    private List<StatementDTO> statement = new ArrayList<>();
    @JsonProperty("Version")
    private String version = "2012-10-17";

    @Data
    @NoArgsConstructor
    public static class StatementDTO {
        @JsonProperty("Action")
        private List<String> action;
        @JsonProperty("Effect")
        private String effect;
        @JsonProperty("Principal")
        private String principal;
        @JsonProperty("Resource")
        private String resource;
    }

    public Policy(String bucketName) {
        StatementDTO statement1 = new StatementDTO();
        statement1.setAction(Collections.singletonList("s3:GetObject"));
        statement1.setPrincipal("*");
        statement1.setResource("arn:aws:s3:::" + bucketName + "/*");
        statement1.setEffect("Allow");
        statement.add(statement1);
    }
}