package com.it.tbh.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author TBH
 * @date 2025-02-06 15:39:40
 * @desc
 */
@Data
@TableName(value = "tb_role")
public class Role implements Serializable {
    @TableId(value = "id")
    private Integer id;

    @TableField(value = "role_name")
    private String roleName;

    @TableField(value = "role_code")
    private String roleCode;

    @TableField(value = "creator",fill = FieldFill.INSERT)
    private Integer creator;

    @TableField(value = "updater",fill = FieldFill.INSERT_UPDATE)
    private Integer updater;

    @TableField(value = "created",fill = FieldFill.INSERT)
    private Date created;

    @TableField(value = "updated",fill = FieldFill.INSERT_UPDATE)
    private Date updated;

    @TableLogic
    @TableField(value = "deleted",fill = FieldFill.INSERT)
    private Boolean deleted;

}