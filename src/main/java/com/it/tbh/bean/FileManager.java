package com.it.tbh.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author TBH
 * @date 2025-02-07 11:15:49
 * @desc
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "tb_file_manager")
public class FileManager implements Serializable {
    @TableId(value = "id")
    private Integer id;

    @TableField(value = "main_id")
    private Integer mainId;

    @TableField(value = "file_name")
    private String fileName;

    @TableField(value = "file_type")
    private String fileType;

    @TableField(value = "file_size")
    private Integer fileSize;

    @TableField(value = "url")
    private String url;

    @TableField(value = "private_url")
    private String privateUrl;

    @TableField(value = "public_url")
    private String publicUrl;

    @TableField(value = "domain_url")
    private String domainUrl;

    @TableField(value = "creator")
    private Integer creator;

    @TableField(value = "updater")
    private Integer updater;

    @TableField(value = "created")
    private Date created;

    @TableField(value = "updated")
    private Date updated;

    @TableLogic
    @TableField(value = "deleted")
    private Boolean deleted;

    private static final long serialVersionUID = 1L;
}