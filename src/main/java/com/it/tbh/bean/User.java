package com.it.tbh.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author TBH
 * @date 2025-02-06 15:39:32
 * @desc
 */
@Data
@TableName(value = "tb_user")
public class User implements Serializable {
    @TableId(value = "id")
    private Integer id;

    @TableField(value = "`name`")
    private String name;

    @TableField(value = "username")
    private String username;

    @TableField(value = "sex")
    private Integer sex;

    @TableField(value = "`password`")
    private String password;

    @TableField(value = "sid")
    private String sid;

    @TableField(value = "head")
    private String head;

    @TableField(value = "creator",fill = FieldFill.INSERT)
    private Integer creator;

    @TableField(value = "updater",fill = FieldFill.INSERT_UPDATE)
    private Integer updater;

    @TableField(value = "created",fill = FieldFill.INSERT)
    private Date created;

    @TableField(value = "updated",fill = FieldFill.INSERT_UPDATE)
    private Date updated;

    @TableLogic
    @TableField(value = "deleted",fill = FieldFill.INSERT)
    private Boolean deleted;

}