package com.it.tbh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author TBH
 * @date 2025-02-06 10:56:52
 * @desc
 */
@EnableAspectJAutoProxy
@SpringBootApplication
public class JxtSchoolApplication {
    public static void main(String[] args) {
        SpringApplication.run(JxtSchoolApplication.class, args);
    }
}