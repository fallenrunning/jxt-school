package com.it.tbh.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author TBH
 * @date 2025-02-06 14:22:53
 * @desc 结果封装
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result implements Serializable {

    private boolean success;

    private Integer code;

    private String msg;

    private Object data;

    public static Result OK() {
        return new Result(true, 200, "OK", null);
    }

    public static Result ERROR() {
        return new Result(false, 500, "SERVER ERROR!", null);
    }

    public static Result OK(Object data) {
        return new Result(true, 200, "OK", data);
    }

    public static Result OK(CodeType codeType) {
        return new Result(true, codeType.code, codeType.msg, null);
    }

    public static Result OK(CodeType codeType, Object data) {
        return new Result(true, codeType.code, codeType.msg, data);
    }

    public static Result ERROR(String msg) {
        return new Result(false, 500, msg, null);
    }

    public static Result ERROR(CodeType codeType) {
        return new Result(false, codeType.code, codeType.msg, null);
    }

    @Getter
    public enum CodeType {
        SUCCESS(200, "OK"),
        ERROR(500, "SERVER ERROR!");

        private final int code;
        private final String msg;

        CodeType(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
    }
}
