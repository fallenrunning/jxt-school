package com.it.tbh.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author TBH
 * @date 2025-02-06 17:48:00
 * @desc
 */
@Data
public class MinioResult implements Serializable {

    /**
     * 返回地址
     */
    private String defaultUrl;

    /**
     * 内网地址(如果配置了内网ip)
     */
    private String privateUrl;

    /**
     * 公网地址(如果配置了公网ip)
     */
    private String publicUrl;

    /**
     * 域名地址(如果配置了域名)
     */
    private String domainUrl;

}