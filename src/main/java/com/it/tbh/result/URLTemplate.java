package com.it.tbh.result;

import cn.hutool.core.util.StrUtil;
import com.it.tbh.config.MinioAutoProperties;

import java.io.Serializable;
import java.util.Map;

/**
 * @author TBH
 * @date 2025-02-06 17:48:22
 * @desc
 */
public class URLTemplate implements Serializable {

    public static final String DEFAULT_TEMPLATE = "{url}/{bucket}/{objectName}";
    public static final String STR_DEFAULT_TEMPLATE = "{}/{}/{}";
    public static final String TEMPLATE = "http://{ip}/{bucket}/{objectName}";
    public static final String STR_TEMPLATE = "http://{}/{}/{}";

    public static String defaultUrl(Map<String, String> paramMap) {
        return StrUtil.format(DEFAULT_TEMPLATE, paramMap);
    }

    public static String url(Map<String, String> paramMap) {
        return StrUtil.format(TEMPLATE, paramMap);
    }

    public static String defaultUrl(String url, String bucket, String objectName) {
        return StrUtil.format(STR_DEFAULT_TEMPLATE, url, bucket, objectName);
    }

    public static String url(String ip, String bucket, String objectName) {
        return StrUtil.format(STR_TEMPLATE, ip, bucket, objectName);
    }


    public static MinioResult result(MinioAutoProperties properties, String bucket, String objectName) {
        MinioResult result = new MinioResult();
        String defaultUrl = defaultUrl(properties.getUrl(), StrUtil.isNotBlank(bucket) ? bucket : properties.getBucket(), objectName);
        result.setDefaultUrl(defaultUrl);
        if (StrUtil.isNotBlank(properties.getPublicIp())) {
            String publicUrl = url(properties.getPublicIp(), StrUtil.isNotBlank(bucket) ? bucket : properties.getBucket(), objectName);
            result.setPublicUrl(publicUrl);
        }
        if (StrUtil.isNotBlank(properties.getPrivateIp())) {
            String privateUrl = url(properties.getPrivateIp(), StrUtil.isNotBlank(bucket) ? bucket : properties.getBucket(), objectName);
            result.setPrivateUrl(privateUrl);
        }
        if (StrUtil.isNotBlank(properties.getDomainName())) {
            String domainUrl = url(properties.getDomainName(), StrUtil.isNotBlank(bucket) ? bucket : properties.getBucket(), objectName);
            result.setDomainUrl(domainUrl);
        }
        return result;
    }
}
