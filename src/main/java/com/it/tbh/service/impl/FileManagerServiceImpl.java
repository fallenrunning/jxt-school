package com.it.tbh.service.impl;

import com.it.tbh.mapper.FileManagerMapper;
import com.it.tbh.service.FileManagerService;
import com.it.tbh.service.MinioService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author TBH
 * @date 2025-02-07 11:23:07
 * @desc
 */
@Service("fileManagerService")
@RequiredArgsConstructor(onConstructor_={@Autowired})
public class FileManagerServiceImpl implements FileManagerService {

    private final MinioService minioService;

    private final FileManagerMapper fileManagerMapper;


}
