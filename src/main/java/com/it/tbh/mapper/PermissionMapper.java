package com.it.tbh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.it.tbh.bean.Permission;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author TBH
 * @date 2025-02-06 15:39:49
 * @desc
 */
@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {
}