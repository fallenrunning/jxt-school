package com.it.tbh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.it.tbh.bean.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author TBH
 * @date 2025-02-06 15:39:32
 * @desc
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}