package com.it.tbh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.it.tbh.bean.Role;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author TBH
 * @date 2025-02-06 15:39:40
 * @desc
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
}