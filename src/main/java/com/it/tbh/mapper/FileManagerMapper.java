package com.it.tbh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.it.tbh.bean.FileManager;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author TBH
 * @date 2025-02-07 11:15:49
 * @desc
 */
@Mapper
public interface FileManagerMapper extends BaseMapper<FileManager> {

}