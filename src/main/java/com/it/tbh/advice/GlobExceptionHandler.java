package com.it.tbh.advice;

import com.it.tbh.exception.BaseException;
import com.it.tbh.exception.ViewException;
import com.it.tbh.result.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author TBH
 * @date 2025-02-06 11:42:48
 * @desc 全局异常处理
 */
@RestControllerAdvice
public class GlobExceptionHandler {
    @ExceptionHandler(BaseException.class)
    public Result baseException(BaseException bex) {
        return Result.ERROR(bex.getMessage());
    }

    @ExceptionHandler(ViewException.class)
    public ModelAndView viewException(ViewException vex){
        ModelAndView view = new ModelAndView();
        view.addObject("msg",vex.getMessage());
        view.setViewName("/error");
        return view;
    }
}