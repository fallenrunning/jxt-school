package com.it.tbh.exception;

/**
 * @author TBH
 * @date 2025-02-08 17:45:11
 * @desc
 */
public class BaseException extends Exception{

    public BaseException(String message){
        super(message);
    }
}
