package com.it.tbh.exception;

/**
 * @author TBH
 * @date 2025-02-08 17:45:18
 * @desc
 */
public class ViewException extends Exception{
    public ViewException(String message){
        super(message);
    }
}
