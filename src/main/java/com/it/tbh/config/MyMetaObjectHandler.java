package com.it.tbh.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "creator", Integer.class, 0);
        this.strictInsertFill(metaObject, "updater", Integer.class, 0);
        this.strictInsertFill(metaObject, "created", Date.class, new Date());
        this.strictInsertFill(metaObject, "updated", Date.class, new Date());
        this.strictInsertFill(metaObject, "deleted", Boolean.class, Boolean.TRUE);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "updater", Integer.class, 0);
        this.strictInsertFill(metaObject, "updated", Date.class, new Date());
    }
}