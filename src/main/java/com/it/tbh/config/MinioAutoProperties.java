package com.it.tbh.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "spring.minio")
public class MinioAutoProperties {

    /**
     * minio地址:完整的http地址 如:http://127.0.0.1:9000
     */
    private String url;

    /**
     * 认证账户可以是root,可以是你用root创建的
     */
    private String accessKey;

    /**
     * 认证密码
     */
    private String secretKey;

    /**
     * 桶名称, 优先级最低
     */
    private String bucket;

    /**
     * 桶不在的时候是否新建桶
     */
    private boolean createBucket = true;

    /**
     * 启动的时候检查桶是否存在
     */
    private boolean checkBucket = true;

    /**
     * 设置HTTP连接、写入和读取超时。值为0意味着没有超时
     * HTTP连接超时，以毫秒为单位。
     */
    private long connectTimeout;

    /**
     * 设置HTTP连接、写入和读取超时。值为0意味着没有超时
     * HTTP写超时，以毫秒为单位。
     */
    private long writeTimeout;

    /**
     * 设置HTTP连接、写入和读取超时。值为0意味着没有超时
     * HTTP读取超时，以毫秒为单位。
     */
    private long readTimeout;

    /**
     * 是否默认开启授权,默认为是,不开启需要自己去minio控制台设置,否则前端访问不到
     */
    private boolean isDefaultPolicy = true;
    /**
     * 授权字符串,在官网描述是固定格式样板,但是需要JSON字符串,此处传入时,即自定义,
     * 此项配置优先级大于默认配置,也就是说如果传入了自己的授权,那么上边的isDefaultPolicy
     * 将会失去作用
     */
    private String policy;

    /**
     * 内网ip 内网ip如果有端口,请带上端口 如:127.0.0.1:9000
     */
    private String privateIp;

    /**
     * 公网ip 公网ip如果有端口,请带上端口 如:1.1.1.1:9000
     */
    private String publicIp;

    /**
     * 域名,如果域名配置的minio有端口,那么请带上端口 如:www.baidu.com:9000
     */
    private String domainName;

}