package com.it.tbh;

import com.it.tbh.mapper.UserMapper;
import com.it.tbh.result.MinioResult;
import com.it.tbh.service.MinioService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

/**
 * @author TBH
 * @date 2025-02-06 11:04:00
 * @desc
 */
@SpringBootTest
public class JxtSchoolApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private MinioService minioService;

    @Test
    void contextLoads() {
        MinioResult minioResult = minioService.putObject(new File("E:/20241205214158.jpg"));
        System.out.println(minioResult);
    }

}
